#non
import nvwave
import globalPluginHandler
import globalVars
import sys, os
import threading 
import tones
import time
PLUGIN_DIR = os.path.dirname(__file__)

globalVars.message=""
globalVars.lock = threading.Lock()
class GlobalPlugin(globalPluginHandler.GlobalPlugin):
	def __init__(self):		
		self.d=threading.Thread(target=check)
		#self.d.daemon = True
		super(globalPluginHandler.GlobalPlugin, self).__init__()
		self.d.start()
	
	def terninate(self):
		with globalVars.lock:
			globalVars.message="die"
		return


def check():
	#Monkeypatch the nvwave function for playing wave files so we can tell the silence to stop otherwise nvwave cuts off the audio in and it sounds bad.
	nvwave.oldPlayWaveFile = nvwave.playWaveFile
	def playWaveFile(sound, async=True):
		with globalVars.lock:
			if sound == "waves\\exit.wav":
				globalVars.message="die"
			else:
				globalVars.message="wait"
		time.sleep(.01)
		count=0
		while count<=1000000: #Fail safe.
			if globalVars.message=="":
				if async: #schedule a new thread to start silence when the file stops.
					def run():
						with globalVars.lock:
							nvwave.oldPlayWaveFile(sound, False)
							if sound != "waves\\exit.wav":
								globalVars.message="resume"
					threading.Thread(target=run).start()
				else: # lock the main thread up.
					with globalVars.lock:
						nvwave.oldPlayWaveFile(sound, False)
						globalVars.message = "resume"
				break
			else:
				count+=1
	nvwave.playWaveFile = playWaveFile
	noSound=False
	time.sleep(1) #give time to finish playing any audio currently playing (start.wav) ? 
	while True:
		time.sleep(.0125) #hopefully this gives the main thread time to acquire the lock.
		with globalVars.lock:
			message= globalVars.message
			if message== "die":
				globalVars.message=""
				break
			elif message == "wait":
				noSound=True
				globalVars.message=""
				continue
			elif message == "resume":
				noSound=False
				globalVars.message=""
				continue
		if not noSound:
			nvwave.oldPlayWaveFile(os.path.join(PLUGIN_DIR, "silence.wav"), async=False)
